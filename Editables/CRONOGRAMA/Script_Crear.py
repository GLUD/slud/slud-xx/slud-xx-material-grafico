import os
import cairosvg

def crear_imagenes(entrada,salida):
    # Ruta de la carpeta con archivos SVG y carpeta de salida para los PNG
    carpeta_entrada = entrada
    carpeta_salida = salida

    # Asegurémonos de que la carpeta de salida exista
    if not os.path.exists(carpeta_salida):
        os.makedirs(carpeta_salida)

    # Lista todos los archivos SVG en la carpeta de entrada
    archivos_svg = [archivo for archivo in os.listdir(carpeta_entrada) if archivo.endswith(".svg")]

    # Convierte cada archivo SVG a PNG con el mismo nombre
    for archivo_svg in archivos_svg:
        nombre_sin_extension = os.path.splitext(archivo_svg)[0]
        archivo_svg_ruta = os.path.join(carpeta_entrada, archivo_svg)
        archivo_png_ruta = os.path.join(carpeta_salida, f"{nombre_sin_extension}.png")
        cairosvg.svg2png(url=archivo_svg_ruta, write_to=archivo_png_ruta)
        print(f"Convertido: {archivo_svg} -> {nombre_sin_extension}.png")

crear_imagenes("./","imagen")

print("Proceso completado")
