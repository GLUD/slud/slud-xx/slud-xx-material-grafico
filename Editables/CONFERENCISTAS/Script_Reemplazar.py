import csv
import re
import xml.etree.ElementTree as ET


def reemplazar_en_archivo_svg(
    archivo_entrada, archivo_salida, cadena_buscar, cadena_reemplazar
):
    try:
        with open(archivo_entrada + ".svg", "r") as entrada:
            contenido = entrada.read()

            # Utiliza la función sub de re para reemplazar la cadena
            nuevo_contenido = re.sub(
                cadena_buscar, cadena_reemplazar, contenido, flags=re.DOTALL
            )

        with open(archivo_salida + ".svg", "w") as salida:
            salida.write(nuevo_contenido)

        print(
            f'Se ha reemplazado "{cadena_buscar}" con "{cadena_reemplazar}" en el archivo SVG.'
        )

    except Exception as e:
        print(f"Ocurrió un error: {str(e)}")


def corregir_caracteres_para_archivo(cadena):
    # Expresión regular para encontrar caracteres no permitidos en nombres de archivos
    caracteres_no_permitidos = r'[\/:*?"<>|]'

    # Reemplazar caracteres no permitidos con guiones bajos
    cadena_corregida = re.sub(caracteres_no_permitidos, "_", cadena)

    # Reemplazar espacios en blanco con guiones bajos
    cadena_corregida = cadena_corregida.replace(" ", "_")

    return cadena_corregida


# Abre el archivo CSV y lee los datos
with open("datos.csv", "r") as csvfile:
    reader = csv.DictReader(csvfile, delimiter=";")
    for row in reader:
        autor = row["autor"]
        conferencia = row["conferencia"]
        hora = row["hora"]
        fecha = row["fecha"]
        descripcion = row["descripcion"]

        nombre_archivo_salida = corregir_caracteres_para_archivo(autor)

        archivo_entrada_cuadrado = "CONFERENCISTA_CUADRADO"
        archivo_entrada_horizontal = "CONFERENCISTA_HORIZONTAL"

        # Creacion archivo SVG Cuadrado
        archivo_salida_cuadrado = "cuadrado/Cuadrado_" + nombre_archivo_salida

        reemplazar_en_archivo_svg(
            archivo_entrada_cuadrado, archivo_salida_cuadrado, "AUTOR", autor
        )
        reemplazar_en_archivo_svg(
            archivo_salida_cuadrado, archivo_salida_cuadrado, "CONFERENCIA", conferencia
        )
        reemplazar_en_archivo_svg(
            archivo_salida_cuadrado, archivo_salida_cuadrado, "HORA", hora
        )
        reemplazar_en_archivo_svg(
            archivo_salida_cuadrado, archivo_salida_cuadrado, "FECHA", fecha
        )
        reemplazar_en_archivo_svg(
            archivo_salida_cuadrado, archivo_salida_cuadrado, "DESCRIPCION", descripcion
        )

        # Creacion archivo SVG Horizontal
        archivo_salida_horizontal = "horizontal/Horizontal_" + nombre_archivo_salida

        reemplazar_en_archivo_svg(
            archivo_entrada_horizontal, archivo_salida_horizontal, "AUTOR", autor
        )
        reemplazar_en_archivo_svg(
            archivo_salida_horizontal, archivo_salida_horizontal, "CONFERENCIA", conferencia
        )
        reemplazar_en_archivo_svg(
            archivo_salida_horizontal, archivo_salida_horizontal, "HORA", hora
        )
        reemplazar_en_archivo_svg(
            archivo_salida_horizontal, archivo_salida_horizontal, "FECHA", fecha
        )
        reemplazar_en_archivo_svg(
            archivo_salida_horizontal, archivo_salida_horizontal, "DESCRIPCION", descripcion
        )


print(
    "Proceso completado. Los archivos SVG se han generado con los datos del archivo CSV."
)
