import os
import cairosvg

def crear_imagenes(entrada,salida):
    # Ruta de la carpeta con archivos SVG y carpeta de salida para los PNG
    carpeta_svg = entrada
    carpeta_pdf = salida

    # Asegurémonos de que la carpeta de salida exista
    if not os.path.exists(carpeta_pdf):
        os.makedirs(carpeta_pdf)

    # Lista todos los archivos SVG en la carpeta de entrada
    archivos_svg = [archivo for archivo in os.listdir(carpeta_svg) if archivo.endswith(".svg")]

    # Convierte cada archivo SVG a PNG con el mismo nombre
    for archivo in archivos_svg:
        archivo_svg = os.path.join(carpeta_svg, archivo)
        archivo_pdf = os.path.join(carpeta_pdf, os.path.splitext(archivo)[0] + '.pdf')

        try:
            cairosvg.svg2pdf(url=archivo_svg, write_to=archivo_pdf)
            print(f'Convertido: {archivo_svg} -> {archivo_pdf}')
        except Exception as e:
            print(f'Error al convertir {archivo_svg}: {str(e)}')

crear_imagenes("conferencistas","conferencistas/imagen")

print("Proceso completado")
