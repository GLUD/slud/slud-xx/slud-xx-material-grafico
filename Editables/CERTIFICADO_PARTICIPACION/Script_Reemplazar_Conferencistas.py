import csv
import re
import xml.etree.ElementTree as ET


def reemplazar_en_archivo_svg(
    archivo_entrada, archivo_salida, cadena_buscar, cadena_reemplazar
):
    try:
        with open(archivo_entrada + ".svg", "r") as entrada:
            contenido = entrada.read()

            # Utiliza la función sub de re para reemplazar la cadena
            nuevo_contenido = re.sub(
                cadena_buscar, cadena_reemplazar, contenido, flags=re.DOTALL
            )

        with open(archivo_salida + ".svg", "w") as salida:
            salida.write(nuevo_contenido)

        print(
            f'Se ha reemplazado "{cadena_buscar}" con "{cadena_reemplazar}" en el archivo SVG.'
        )

    except Exception as e:
        print(f"Ocurrió un error: {str(e)}")


def corregir_caracteres_para_archivo(cadena):
    # Expresión regular para encontrar caracteres no permitidos en nombres de archivos
    caracteres_no_permitidos = r'[\/:*?"<>|]'

    # Reemplazar caracteres no permitidos con guiones bajos
    cadena_corregida = re.sub(caracteres_no_permitidos, "_", cadena)

    # Reemplazar espacios en blanco con guiones bajos
    cadena_corregida = cadena_corregida.replace(" ", "")

    return cadena_corregida


# Abre el archivo CSV y lee los datos
def reemplazar_contenido(archivo_datos,archivo_plantilla,carpeta_destino,columna_principal):
    with open(archivo_datos, "r") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")
        for row in reader:
            nombre_archivo_salida = corregir_caracteres_para_archivo(row[columna_principal])
            archivo_entrada = archivo_plantilla

            # Creacion archivo SVG Cuadrado
            archivo_salida = carpeta_destino+"/Cuadrado_" + nombre_archivo_salida

            for llave in  row.keys():
                valor = row[llave]
                print('Valor: '+valor+'. Llave: '+ llave)

                reemplazar_en_archivo_svg(
                    archivo_entrada, archivo_salida, llave, valor
                )
                archivo_entrada = archivo_salida
 
reemplazar_contenido("datos.csv","CERTIFICADO","conferencistas","nombre_persona")

print(
    "Proceso completado. Los archivos SVG se han generado con los datos del archivo CSV."
)
