# SLUD-XX-Material-Grafico

En este repositorio se manejaran los editables y entregables de los materiales graficos para el SLUD XIX.

## Plan grafico
Documento donde se indica los entregables a realizar para la SLUD XX [Plan Grafico](https://udistritaleduco-my.sharepoint.com/:w:/g/personal/glud_udistrital_edu_co/EXFw_fxPvIBAu_vvVrxqNaUBXhD3UdUDI2cV5x5GBfVHLg?e=OydXKw)


## Plan publicitario

Documento donde se indica las publicaciones a realizar para la SLUD XX [Plan Publicitario](https://udistritaleduco-my.sharepoint.com/:x:/r/personal/glud_udistrital_edu_co/Documents/GLUD/SLUD%20-%20Hackathon/2023%20SLUD%20XX/Material%20Grafico/Cronograma%20Publicaciones.xlsx?d=wdfd0ee4c641f43c09a4e65611145878a&csf=1&web=1&e=uOxJGp)

# Orden del repositorio

## Entregables

Se ubican los archivos que estan listos para publicar o hacer uso se ellos.

## Editables

Se ubican los archivos editables de diseño.

## Referencias_diseño

Se ubican la presentación de las bases para los diseños: Paleta, logo, elementos, tipografia...

La idea es mantener una uniformidad de todos los entregables. Es posible modificar las bases sin ningun problema, si se desean generalizar, los editables de las bases se encuentran en ***Editables***.

# Links utiles

## Figma (Actual)
[Figma SLUD XX](https://www.figma.com/file/0cJgrw0DCR37vJPGsLYFGQ/SLUD_XX?node-id=1-3&t=jWulZmFuP6zaSvnb-0)

## Figma
[Figma SLUD XIX](https://www.figma.com/file/6iafVULoGeJwvb1qb2mxFk/Pagina-SLUD?node-id=0-1&t=jWulZmFuP6zaSvnb-0)

## Repositorio SLUD XIX

[Repositorio SLUD XIX](https://gitlab.com/GLUD/slud/slud-xix/slud-xix-material-grafico)

## Repositorio SLUD XVIII

[Repositorio SLUD XVIII](https://gitlab.com/GLUD/slud/slud-xviii/slud-xviii-disenos)

## OneDrive ArtGlud 2023-1

[One Drive ArtGlud 2023-1](https://udistritaleduco-my.sharepoint.com/personal/spinillap_udistrital_edu_co/_layouts/15/onedrive.aspx?login_hint=spinillap%40udistrital%2Eedu%2Eco&id=%2Fpersonal%2Fglud%5Fudistrital%5Fedu%5Fco%2FDocuments%2FGLUD%2FProyectos%2FART%20GLUD%2F2023%2DI&listurl=%2Fpersonal%2Fglud%5Fudistrital%5Fedu%5Fco%2FDocuments&remoteItem=%7B%22mp%22%3A%7B%22webAbsoluteUrl%22%3A%22https%3A%2F%2Fudistritaleduco%2Dmy%2Esharepoint%2Ecom%2Fpersonal%2Fspinillap%5Fudistrital%5Fedu%5Fco%22%2C%22listFullUrl%22%3A%22https%3A%2F%2Fudistritaleduco%2Dmy%2Esharepoint%2Ecom%2Fpersonal%2Fspinillap%5Fudistrital%5Fedu%5Fco%2FDocuments%22%2C%22rootFolder%22%3A%22%2Fpersonal%2Fspinillap%5Fudistrital%5Fedu%5Fco%2FDocuments%2FUD%2FGlud%2FGLUD%22%7D%2C%22rsi%22%3A%7B%22listFullUrl%22%3A%22https%3A%2F%2Fudistritaleduco%2Dmy%2Esharepoint%2Ecom%2Fpersonal%2Fglud%5Fudistrital%5Fedu%5Fco%2FDocuments%22%2C%22rootFolder%22%3A%22%2Fpersonal%2Fglud%5Fudistrital%5Fedu%5Fco%2FDocuments%2FGLUD%2FProyectos%2FART%20GLUD%2F2023%2DI%22%2C%22webAbsoluteUrl%22%3A%22https%3A%2F%2Fudistritaleduco%2Dmy%2Esharepoint%2Ecom%2Fpersonal%2Fglud%5Fudistrital%5Fedu%5Fco%22%7D%7D&view=0)

